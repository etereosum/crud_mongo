const express = require("express");
const config = require("./config/config");
const routerApi = require("./routes");
const connection = require("./db/connection");

const app = express();
connection();

app.use(express.json());
routerApi(app);

const port = config.PORT;

app.listen(port, () => {
  console.log(`Listen on port: ${port}`);
});
