const routerClients = require("./clients.router");

function routerApi(app) {
  app.use("/api/clients", routerClients);
}

module.exports = routerApi;
