const express = require("express");
const router = express.Router();
const {
  index,
  show,
  store,
  update,
  destroy,
} = require("../controllers/client.controller");

/**
 * List clients
 */

router.get("/", async (req, res) => {
  const clients = await index();
  res.json(clients);
});

/**
 * Show client by id
 */

router.get("/:clientId", async (req, res) => {
  const { clientId } = req.params;
  const client = await show(clientId);
  res.send(client);
});

/**
 * Save client
 */

router.post("/", async (req, res) => {
  const data = req.body;
  const result = await store(data);
  res.send(result);
});

/**
 * Update client
 */

router.put("/:clientId", async (req, res) => {
  const { clientId } = req.params;
  const data = req.body;
  const client = await update(data, clientId);
  res.send(client);
});

/**
 * Delete client
 */

router.delete("/:clientId", async (req, res) => {
  const { clientId } = req.params;
  await destroy(clientId);
  res.send("The client is delete");
});

module.exports = router;
