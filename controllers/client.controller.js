const Client = require("../models/client.model");

async function index() {
  const clients = await Client.find({});
  return clients;
}

async function show(clientId) {
  const client = await Client.findById(clientId).exec();
  return client;
}

async function store(dataClient) {
  let newClient = new Client(dataClient);
  const result = await newClient.save();
  return result;
}

async function update(dataClient, clientId) {
  await Client.findOneAndUpdate({ _id: clientId }, dataClient);
  const client = await Client.findById(clientId);
  return client;
}

async function destroy(clientId) {
  await Client.deleteOne({ _id: clientId });
  return true;
}

module.exports = {
  index,
  show,
  store,
  update,
  destroy,
};
