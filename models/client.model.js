const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let client = new Schema(
  {
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
  },
  {
    collection: "mongoDB",
  }
);

module.exports = mongoose.model("clients", client);
